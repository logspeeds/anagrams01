//Variável do resultado final
let resultadoFinal = "";
let resultado = document.getElementById("resultsDiv")


//função que ordena a palavra digitada pelo usuário e transforma em uma string em ordem alfabetica
function alphabetize(name) {
    return name.toLowerCase().split("").sort().join("").trim();
}

//função que busca um anagrama para a palavra inserida pelo usuário dentro do arquivo "palavras.js"
document.getElementById("searchButton").onclick = function getAnagramsOf() {
    resultadoFinal = "";
    let typedText = document.getElementById("textBox").value;
    let words= alphabetize (typedText);
    // let hhh =  alphabetize (palavras);
    
    for (i = 0; i < palavras.length; i++) {
        console.log(palavras[i])
        let palavras_ordenadas = alphabetize(palavras[i]);
        if (words == palavras_ordenadas) {
            resultadoFinal += palavras[i] + "<br>";
        }
    }

    return resultado.innerHTML = resultadoFinal;
}
